# frozen_string_literal: true

json.collection! 'genres/genre', genre: @genre
