# frozen_string_literal: true

json.extract!(
  contributor,
  :name
)
