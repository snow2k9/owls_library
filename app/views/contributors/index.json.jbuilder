# frozen_string_literal: true

json.array! @contributors, partial: 'contributors/contributor', as: :contributor
