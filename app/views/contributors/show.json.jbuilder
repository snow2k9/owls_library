# frozen_string_literal: true

json.collection! 'contributors/contributor', contributor: @contributor
