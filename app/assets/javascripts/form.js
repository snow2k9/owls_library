// instantiate the bloodhound suggestion engine
var contributors = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  prefetch: '/contributors.json',
  remote: {
    url: '/contributors.json?q=%QUERY',
    wildcard: '%QUERY'
  }
});

function init_typeahead_contributors(object=$('.typeahead.contributor')) {
  // instantiate the typeahead UI
  contributors.initialize();
  
  object.typeahead(null, {
    displayKey: 'name',
    source: contributors.ttAdapter()
  });

  $('#contributors').on('cocoon:after-insert', function (e, insertedItem) {
    init_typeahead_contributors($(insertedItem).find('.typeahead.contributor'))
  })
}

function init_tag_box(argument) {
  $("#tags input.addTag").on({
    focusout : function() {
      var txt = this.value.replace(/[^a-z0-9\+\-\.\#]/ig,''); // allowed characters
      if(txt) {
        $('#tag_list').append('<span class="tag">'+txt+'<span>');
        var tagChildren = document.getElementById('tag_list')
        var newTags = Object.keys(tagChildren).map(function (k,v) {
          return tagChildren[k].innerText
        })
        $('#medium_tags_name').val(newTags.join(','))
      };
      this.value = "";
    },
    keyup : function(ev) {
      // if: comma|enter (delimit more keyCodes with | pipe)
      if(/(188|32)/.test(ev.which)) $(this).focusout(); 
    }
  });
  $('#tag_list').on('click', 'span', function() {
    $(this).remove();
  });
}

$(document).on('turbolinks:load', function () {
  init_typeahead_contributors();
  init_tag_box();
})
