# frozen_string_literal: true

# == Schema Information
#
# Table name: contributions
#
#  id             :bigint(8)        not null, primary key
#  medium_id      :uuid             not null
#  contributor_id :uuid             not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  role           :integer          default("contributor")
#
# Indexes
#
#  index_contributions_on_contributor_id  (contributor_id)
#  index_contributions_on_medium_id       (medium_id)
#
# Foreign Keys
#
#  fk_rails_...  (contributor_id => contributors.id)
#  fk_rails_...  (medium_id => media.id)
#

class Contribution < ApplicationRecord
  enum role: %i[contributor publisher]
  belongs_to :medium
  belongs_to :contributor
end
