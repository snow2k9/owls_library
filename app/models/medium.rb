# frozen_string_literal: true

# == Schema Information
#
# Table name: media
#
#  id          :uuid             not null, primary key
#  title       :string           not null
#  code        :string           not null
#  release     :date             not null
#  description :text
#  genre_id    :bigint(8)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  category    :integer          default("book")
#
# Indexes
#
#  index_media_on_genre_id  (genre_id)
#
# Foreign Keys
#
#  fk_rails_...  (genre_id => genres.id)
#

class Medium < ApplicationRecord
  before_save :correct_code

  enum category: %i[book movie series videogame]
  acts_as_taggable
  has_one_attached :cover
  belongs_to :genre

  has_many :contributions, -> { where role: :contributor },
           dependent: :delete_all,
           inverse_of: :medium
  has_many :contributors,
           through: :contributions

  has_one :contribution, -> { where role: :publisher },
          dependent: :delete,
          inverse_of: :medium
  has_one :publisher,
          through: :contribution,
          source: 'contributor'

  validates :title, presence: true
  validates :code, presence: true
  validates :code, uniqueness: true
  validates :release, presence: true
  validates :category, presence: true

  accepts_nested_attributes_for :contributors,
                                reject_if: :all_blank,
                                allow_destroy: true
  accepts_nested_attributes_for :publisher,
                                reject_if: :all_blank

  def build_publisher
    self.publisher = Contributor.new
  end

  def publisher_attributes=(attrs = {})
    self.publisher = Contributor.where(name: attrs[:name].strip).first_or_create
  end

  private

  def correct_code
    self.code = code.gsub(/[^0-9]/, '')
  end
end
