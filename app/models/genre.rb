# frozen_string_literal: true

# == Schema Information
#
# Table name: genres
#
#  id         :bigint(8)        not null, primary key
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Genre < ApplicationRecord
  has_many :media, dependent: :restrict_with_error

  validates :name, presence: true
  validates :name, uniqueness: true
end
