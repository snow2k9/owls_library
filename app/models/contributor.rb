# frozen_string_literal: true

# == Schema Information
#
# Table name: contributors
#
#  id          :uuid             not null, primary key
#  name        :string           not null
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Contributor < ApplicationRecord
  has_many :contributions, dependent: :restrict_with_error
  has_many :media, through: :contributions

  validates :name, presence: true
  validates :name, uniqueness: true
end
