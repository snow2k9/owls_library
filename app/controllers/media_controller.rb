# frozen_string_literal: true

class MediaController < ApplicationController
  before_action :set_medium, only: %i[show edit update destroy]
  before_action :create_medium, only: %i[create]
  before_action :set_category, only: %i[new update]
  before_action :set_tags, only: %i[create update]
  before_action :validates_contributors, only: %i[create update]

  def index
    @media = Medium.eager_load(:genre,
                               :publisher,
                               :contributors,
                               :contributions)
                   .all
  end

  def show; end

  def edit; end

  def new
    @medium = Medium.new(category: @category)
    @medium.build_publisher
    @medium.contributors.build
  end

  def create
    respond_to do |format|
      if @medium.save
        format.html { redirect_to root_path, notice: 'medium was successfully created.' }
      #     format.json { render :new, status: :created, location: @medium }
      else
        format.html { render 'media/new' }
        #     format.json { render json: @medium.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    # logger.debug @medium
    respond_to do |format|
      if @medium.update(medium_params.except(:contributors_attributes, :tag_list)) && @medium.save

        format.html { redirect_to root_path, notice: 'medium was successfully updated.' }
        format.json { render :edit, status: :ok, location: @medium }
      else
        format.html { render :edit }
        format.json { render json: @medium.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @medium.destroy
    respond_to do |format|
      format.html { redirect_to root_path, notice: 'medium was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def set_medium
    @medium = Medium.find(params[:id])
  end

  def medium_params
    params.require(:medium).permit(:title,
                                   :code,
                                   :category,
                                   :release,
                                   :genre_id,
                                   :description,
                                   :tag_list,
                                   publisher_attributes: %i[id name],
                                   contributors_attributes: %i[id name _destroy])
  end

  def create_medium
    @medium = Medium.new(medium_params.except(:contributors_attributes))
  end

  def validates_contributors
    new_contributors = []
    medium_params[:contributors_attributes].each do |_key, contributor|
      next if contributor[:_destroy] == '1' || contributor[:name].blank?

      new_contributors << Contributor
                          .where(name: contributor[:name].strip)
                          .first_or_create
    end
    @medium.contributors = new_contributors
  end

  def set_tags
    @medium.tag_list = medium_params[:tag_list].gsub(/[^a-zA-Z0-9]/,',')
  end

  def set_category
    return not_found unless !Medium.categories.include?(params[:category])

    @category = params[:category]
  end
end
