# frozen_string_literal: true

class ApplicationController < ActionController::Base
  protected

  def unauthorized
    render status: :unauthorized, template: '/errors/unauthorized.html.haml'
    false
  end

  def not_found
    render status: :not_found, template: '/errors/not_found.html.haml'
    false
  end

  def forbidden
    render status: :forbidden, template: '/errors/forbidden.html.haml'
    false
  end
end
