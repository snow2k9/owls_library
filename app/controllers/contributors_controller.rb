# frozen_string_literal: true

class ContributorsController < ApplicationController
  before_action :set_filter, only: %i[index]
  before_action :set_format_json, only: %i[index]

  def index
    @contributors = Contributor.where(Contributor.arel_table[:name].matches("%#{@query}%")).limit(20)
  end

  private

  def set_filter
    @query = params[:q] || ''
  end

  def set_format_json
    request.format = :json
  end
end
