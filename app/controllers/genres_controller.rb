# frozen_string_literal: true

class GenresController < ApplicationController
  before_action :set_filter, only: %i[index]
  before_action :set_format_json, only: %i[index]

  def index
    @genres = Genre.where(Genre.arel_table[:name].matches("%#{@query}%")).limit(20)
  end

  private

  def set_filter
    @query = params[:q] || ''
  end

  def set_format_json
    request.format = :json
  end
end
