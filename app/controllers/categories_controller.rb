# frozen_string_literal: true

class CategoriesController < ApplicationController
  before_action :set_category, only: %i[index]

  def index
    @media = Medium.eager_load(:genre,
                               :publisher,
                               :contributors,
                               :contributions)
                   .where(category: @category)
    render 'media/index'
  end

  private

  def set_category
    return not_found unless Medium.categories.include?(params[:category].singularize)

    @category = params[:category].singularize
  end
end
