# frozen_string_literal: true

Genre.create([
               { name: 'Action' },
               { name: 'Fantasy' },
               { name: 'Strategy' },
               { name: 'History' },
               { name: 'Thriller' },
               { name: 'Simulation' },
               { name: 'Romance' },
               { name: 'Science-Fiction' }
             ])
