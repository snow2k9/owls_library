# frozen_string_literal: true

class AddCategoryToMedia < ActiveRecord::Migration[5.2]
  def change
    add_column :media, :category, :integer, default: 0
  end
end
