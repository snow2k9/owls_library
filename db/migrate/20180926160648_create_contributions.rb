# frozen_string_literal: true

class CreateContributions < ActiveRecord::Migration[5.2]
  def change
    create_table :contributions do |t|
      t.belongs_to :medium, foreign_key: true, type: :uuid, null: false
      t.belongs_to :contributor, foreign_key: true, type: :uuid, null: false

      t.timestamps
    end
  end
end
