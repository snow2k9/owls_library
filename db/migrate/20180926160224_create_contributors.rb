# frozen_string_literal: true

class CreateContributors < ActiveRecord::Migration[5.2]
  def change
    create_table :contributors, id: :uuid do |t|
      t.string :name, null: false
      t.text :description

      t.timestamps
    end
  end
end
