# frozen_string_literal: true

class AddRoleToContributions < ActiveRecord::Migration[5.2]
  def change
    add_column :contributions, :role, :integer, default: 0
  end
end
