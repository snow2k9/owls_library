# frozen_string_literal: true

class CreateMedia < ActiveRecord::Migration[5.2]
  def change
    create_table :media, id: :uuid do |t|
      t.string :title, null: false
      t.string :code, null: false, unique: true
      t.date :release, null: false
      t.text :description
      t.belongs_to :genre, foreign_key: true

      t.timestamps
    end
  end
end
