# frozen_string_literal: true

# == Schema Information
#
# Table name: contributors
#
#  id          :uuid             not null, primary key
#  name        :string           not null
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryBot.define do
  factory :contributor do
    name { 'MyString' }
    description { 'MyText' }
  end
end
