# frozen_string_literal: true

# == Schema Information
#
# Table name: media
#
#  id          :uuid             not null, primary key
#  title       :string           not null
#  code        :string           not null
#  release     :date             not null
#  description :text
#  genre_id    :bigint(8)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  category    :integer          default("book")
#
# Indexes
#
#  index_media_on_genre_id  (genre_id)
#
# Foreign Keys
#
#  fk_rails_...  (genre_id => genres.id)
#

require 'rails_helper'

RSpec.describe Medium, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
