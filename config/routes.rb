# frozen_string_literal: true

# == Route Map
#
#                    Prefix Verb   URI Pattern                                                                              Controller#Action
#                     media POST   /media(.:format)                                                                         media#create
#               edit_medium GET    /media/:id/edit(.:format)                                                                media#edit
#                    medium GET    /media/:id(.:format)                                                                     media#show
#                           PATCH  /media/:id(.:format)                                                                     media#update
#                           PUT    /media/:id(.:format)                                                                     media#update
#                           DELETE /media/:id(.:format)                                                                     media#destroy
#              contributors GET    /contributors(.:format)                                                                  contributors#index
#                    genres GET    /genres(.:format)                                                                        genres#index
#                  category GET    /:category(.:format)                                                                     categories#index
#              new_category GET    /:category/new(.:format)                                                                 media#new
#                      root GET    /                                                                                        users#index
#        rails_service_blob GET    /rails/active_storage/blobs/:signed_id/*filename(.:format)                               active_storage/blobs#show
# rails_blob_representation GET    /rails/active_storage/representations/:signed_blob_id/:variation_key/*filename(.:format) active_storage/representations#show
#        rails_disk_service GET    /rails/active_storage/disk/:encoded_key/*filename(.:format)                              active_storage/disk#show
# update_rails_disk_service PUT    /rails/active_storage/disk/:encoded_token(.:format)                                      active_storage/disk#update
#      rails_direct_uploads POST   /rails/active_storage/direct_uploads(.:format)                                           active_storage/direct_uploads#create

Rails.application.routes.draw do
  resources :media, except: %i[index new]
  resources :contributors, only: [:index]
  resources :genres, only: [:index]

  get ':category', controller: :categories, action: :index, as: :category
  get ':category/new', controller: :media, action: :new, as: :new_with_category

  root 'users#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
